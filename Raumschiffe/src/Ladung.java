public class Ladung {

    public String typ;
    public int menge;

    /**
     * @param typ_objekt
     * @param menge_objekt
     */
    Ladung( String typ_objekt, int menge_objekt)
    {
        typ = typ_objekt;
        menge = menge_objekt;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }
}
