public class Kapitean {

    public String name;
    public int dienstbeginn;

    /**
     * @param name_objekt
     * @param dienstbeginn_objekt
     */
    Kapitean( String name_objekt, int dienstbeginn_objekt)
    {
        name = name_objekt;
        dienstbeginn = dienstbeginn_objekt;
    }

    public int getDienstbeginn() {
        return dienstbeginn;
    }

    public void setDienstbeginn(int dienstbeginn) {
        this.dienstbeginn = dienstbeginn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
