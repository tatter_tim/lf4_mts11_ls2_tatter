import java.io.*;
import java.util.*;
import java.util.Random;

/**
 * @author Tim Tatter
 * @version 1.0
 */
public class Raumschiff {

    public String name;
    public float schutzschilde;
    public float lebenserhaltungssysteme;
    public float huelle;
    public float energieversorgung;
    public int reperaturAndroid;
    public int photonentorpedos;
    static ArrayList<String> broadcastKommunikator = new ArrayList<>();
    public ArrayList<Ladung> ladungsverzeichnis = new ArrayList<>();


    /**
     * @param name
     * @param schutzschilde
     * @param lebenserhaltungssysteme
     * @param huelle
     * @param energieversorgung
     * @param reperaturAndroid
     * @param photonentorpedo
     */
    public Raumschiff(String name, float schutzschilde, float lebenserhaltungssysteme, float huelle, float energieversorgung, int reperaturAndroid, int photonentorpedo) {
        this.name = name;
        this.schutzschilde = schutzschilde;
        this.lebenserhaltungssysteme = lebenserhaltungssysteme;
        this.huelle = huelle;
        this.energieversorgung = energieversorgung;
        this.reperaturAndroid = reperaturAndroid;
        this.photonentorpedos = photonentorpedo;
    }

    /** Die Funktion photonentorpedoAbschuss feuert, wenn vorhanden, einen Photonentorpedo ab, entfernt für jeden Schuss
     * einen aus dem Ladungsverzeichnis sowie gibt für jeden eine Nachricht an den Broadcast Kommunikator weiter.*/
    public void photonentorpedoAbschuss( Raumschiff ziel, int anzahlTorpedos ){
        if ( this.photonentorpedos <= 0 ){
            this.sendeNachrichtAnAlle("-=*CLick*=-");
        }else{
            for ( int y = 1; y < anzahlTorpedos; y++ ){
                for ( int x = 0; x < this.ladungsverzeichnis.size(); x++ ){
                    if (this.ladungsverzeichnis.get(x).getTyp() == "Photonentorpedo"){
                        if (this.ladungsverzeichnis.get(x).getMenge() == 0) {
                            this.ladungsverzeichnis.get(x).setMenge(this.ladungsverzeichnis.get(x).getMenge() - 1);
                            this.sendeNachrichtAnAlle("Photonentorpedo abgeschossen");
                        }else{
                            this.sendeNachrichtAnAlle("Das Raumschiff hat keinen weiteren Photonentorpedo!");
                        }
                    }
                }
            }
            ziel.raumschiffGetroffen();
        }
    }

    /** Die Funktion PhaserkanonenAbschuss feuert, wenn Energie vorhanden, auf ein Schiff, verringert die eigene
     * Energieversorgung um 50% und sendet eine Nachricht an das Protocol des Broadcast Kommunikators.*/
    public void phaserkanonenAbschuss( Raumschiff ziel ){
        if ( this.energieversorgung <= 0.5 ){
            this.sendeNachrichtAnAlle("-=*CLick*=-");
        }else{
            this.energieversorgung -= 0.5;
            this.sendeNachrichtAnAlle("Phaserkanone abgeschossen");
            ziel.raumschiffGetroffen();
        }
    }

    /** Die Funktion trefferVermerken gibt bei erfolgreichen Treffer eine Nachricht an den Broadcast Kommunikator weiter.*/
    public void trefferVermerken( Raumschiff getroffenesRaumschiff ){
        broadcastKommunikator.add(getroffenesRaumschiff.getName() + "wurde getroffen!");
    }

    /** Die Funktion raumschiffGetroffen verringert bei Treffer zuerst die Schilde dann die Huelle und Energieversorgung so oft
     * bis die Lebenserhaltungssysteme ebenfalls zerstört werden und eine Bestätigung an den Broadcast Kommunikator weitergegeben wird.*/
    private void raumschiffGetroffen(){
        if ( this.schutzschilde > 0 ) {
            this.schutzschilde -= 0.5;
            if ( this.schutzschilde <= 0 ){
                this.energieversorgung -= 0.5;
                this.huelle -= 0.5;
            }
        }else{
            if ( this.huelle > 0 ){
                this.energieversorgung -= 0.5;
                this.huelle -= 0.5;
            }else{
                this.sendeNachrichtAnAlle("Lebenserhaltungssysteme von" + this.getName() + "wurden zerstört!!!");
            }
        }
    }

    /** Die Funktion sendNachrichtAnAlle gibt einkommende Nachricht an den Broadcast Kommunikator weiter.*/
    public void sendeNachrichtAnAlle( String nachricht ){
        broadcastKommunikator.add(nachricht);
    }

    /** Die Funktion beladeRaumschiff fügt dem Ladungsverzeichnis das übergebene Objekt des Typs Ladung hinzu.*/
    public void beladeRaumschiff( Ladung neueLadung ){
        this.ladungsverzeichnis.add(neueLadung);
    }

    /** Die Funktion gebeLogbuchAus printed in der Konsole das Protocol des Logbuchs aus.*/
    public void gebeLogbuchAus(){
        for (int i = 0; i < broadcastKommunikator.size(); i++){
            System.out.print(broadcastKommunikator.get(i) + "\n");
        }
    }

    /** Die Funktion ladePhotonentorpedos fügt dem Ladungsverzeichnis des angegebenen Raumschiffs eine Ladung Photonentorpedos hinzu. */
    public void ladePhotonentorpedos( int anzahlTorpedos ){
        Ladung neueLadung = new Ladung("Photonentorpedo", anzahlTorpedos);
        this.ladungsverzeichnis.add(neueLadung);
    }

    /** Die Funktion gebeReperaturBefehl ermittelt die Anzahl an zu "reparierenden" Attributen mit checkWert fragt ab, ob mehr oder weniger Androiden
     * vorhanden sind wie angefordert und berechnet demnach mit einer zufälligen ganzen Zahl zwischen 0 und 100 den reperaturWert der geforderten
     * Attribute des Raumschiffs und fügt diesen den Attributen hinzu.*/
    public void gebeReperaturBefehl( boolean checkSchutzschilde, boolean checkHuelle, boolean checkEnergieversorgung, int anzahlAndroiden ){
        float checkWert = 0;
        if (checkSchutzschilde){checkWert += 1;}
        if (checkHuelle){checkWert += 1;}
        if (checkEnergieversorgung){checkWert += 1;}
        if (anzahlAndroiden <= this.reperaturAndroid){
            Random zufallsZahl = new Random();
            int zufallsZahlZwei = zufallsZahl.nextInt(100);
            float reperaturWert = (zufallsZahlZwei * anzahlAndroiden) / checkWert;
            if (checkSchutzschilde){
                this.schutzschilde += reperaturWert;
            }
            if (checkEnergieversorgung){
                this.energieversorgung += reperaturWert;
            }
            if (checkHuelle){
                this.huelle += reperaturWert;
            }
        }else{
            Random zufallsZahl = new Random();
            int zufallsZahlZwei = zufallsZahl.nextInt(100);
            float reperaturWert = (zufallsZahlZwei * this.reperaturAndroid) / checkWert;
            if (checkSchutzschilde){
                this.schutzschilde += reperaturWert;
            }
            if (checkEnergieversorgung){
                this.energieversorgung += reperaturWert;
            }
            if (checkHuelle){
                this.huelle += reperaturWert;
            }
        }
    }

    /** Die Funktion gebeZustand aus printed alle Attribute des angegebenen Raumschiffs in der Konsole aus.*/
    public void gebeZustandAus(){
        System.out.print("Name:" + this.name + ",\n" + "Schutzschilde:" + this.schutzschilde + ",\n" + "Lebenserhaltungssysteme:" + this.lebenserhaltungssysteme + ",\n" + "Huelle:" + this.huelle + ",\n" + "Energieversorgung:" + this.energieversorgung + ",\n" + "Reperatur Andoroiden:" + this.reperaturAndroid + ",\n" + "Photonentorpedos:" + this.photonentorpedos + "\n");
    }

    /** Die Funktion ausgabeLadungsverzeichnis printed das Ladungsverzeichnis in des angebenen Raumschiffs in der Konsole aus.*/
    public void ausgabeLadungsverzeichnis(){
        for (int x = 0; this.ladungsverzeichnis.size() > x; x++){
            System.out.print(this.ladungsverzeichnis.get(x).getTyp());
        }
    }

    /** Die Funktion ladungsverzeichnisAufraeumen entfernt aus dem Ladungsverzeichnis alle Objekte bei denen die Ladungs Menge Null entspricht.*/
    public void ladungsverzeichnisAufraeumen(){
        for ( int x = 0; x < this.ladungsverzeichnis.size(); x++){
            if ( this.ladungsverzeichnis.get(x).getMenge() == 0 ){
                this.ladungsverzeichnis.remove(x);
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSchutzschilde() {
        return schutzschilde;
    }

    public void setSchutzschilde(float schutzschilde) {
        this.schutzschilde = schutzschilde;
    }

    public float getLebenserhaltungssysteme() {
        return lebenserhaltungssysteme;
    }

    public void setLebenserhaltungssysteme(float lebenserhaltungssysteme) {
        this.lebenserhaltungssysteme = lebenserhaltungssysteme;
    }

    public float getHuelle() {
        return huelle;
    }

    public void setHuelle(float huelle) {
        this.huelle = huelle;
    }

    public float getEnergieversorgung() {
        return energieversorgung;
    }

    public void setEnergieversorgung(float energieversorgung) {
        this.energieversorgung = energieversorgung;
    }

    public int getReperaturAndroid() {
        return reperaturAndroid;
    }

    public void setReperaturAndroid(int reperaturAndroid) {
        this.reperaturAndroid = reperaturAndroid;
    }

    public int getPhotonentorpedo() {
        return photonentorpedos;
    }

    public void setPhotonentorpedo(int photonentorpedo) {
        this.photonentorpedos = photonentorpedo;
    }

    public ArrayList getBroadcastKommunikator() {
        return broadcastKommunikator;
    }

    public void setBroadcastKommunikator(ArrayList broadcastKommunikator) {
        this.broadcastKommunikator = broadcastKommunikator;
    }

    public ArrayList getLadungsverzeichnis() {
        return ladungsverzeichnis;
    }

    public void setLadungsverzeichnis(ArrayList ladungsverzeichnis) {
        this.ladungsverzeichnis = ladungsverzeichnis;
    }
}
