package math;

public class MyMath {
    public static boolean isPrim (long zahl){
        if (zahl < 2){
            return false;
        }
        for ( int x = 2; x < zahl; x++){
            if ( zahl % x == 0){
                return false;
            }
        }
        return true;
    }
}
