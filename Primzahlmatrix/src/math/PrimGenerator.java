package math;

public class PrimGenerator {
    public static boolean[] generatePrimNumbers(long number){
        boolean[] booleanPrimNumbersArray = new boolean[10000];
        for ( int x = 0 ; x < 10000; x++){
            booleanPrimNumbersArray[x] = MyMath.isPrim(number+x);
        }
        return booleanPrimNumbersArray;
    }
}
